package com.example.zyraphyl.userlocator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Locate {
    private Context context;
    private String query;
    private SharedPreferences sharedPref;
    private JSONObject json;
    public void Locate(Context context, String query){
        this.context = context;
        this.query = query;
        json = new JSONObject();
        try {
            json.put("Query",query);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        submitToServer();
        sharedPref = context.getSharedPreferences("Preferences", 0);
    }
    public String sendData(String json){
        String address = sharedPref.getString("Server_Address",null).concat("/locate.php");
        HttpPost post = new HttpPost(address);
        try {
            Log.wtf("sendData","sent");
            StringEntity stringEntity = new StringEntity(json);
            post.setEntity(stringEntity);
            post.setHeader("Content-type","application/json");

            DefaultHttpClient client = new DefaultHttpClient();
            BasicResponseHandler handler = new BasicResponseHandler();

            String resp = client.execute(post,handler);
            Log.wtf("sendData","response:".concat(resp));
            return resp;
        } catch (UnsupportedEncodingException e) {
            Log.d("SendingJson",e.toString());
        } catch (ClientProtocolException e) {
            Log.d("SendingJson",e.toString());
        } catch (IOException e) {
            Log.d("SendingJson",e.toString());
        }
        return "Unable to connect to server";

    }
    private void submitToServer() {

        final String finalJson = json.toString();
//
        new AsyncTask<Void,Void,String>(){

            @Override
            protected String doInBackground(Void... voids) {
                return sendData(finalJson);
            }

            @Override
            protected void onPostExecute(String s) {
                setText(s);

            }
        }.execute();

    }
    public void setText(String text){
        TextView txtView = (TextView) ((Activity)context).findViewById(R.id.response);
        txtView.setText(text);
    }
}
