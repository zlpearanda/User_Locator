package com.example.zyraphyl.userlocator;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Gather extends AppCompatActivity implements View.OnClickListener {
    private WifiReceiver wifi_receiver;
    private WifiManager wifi_manager;
    private WifiInfo info;
    private ArrayList<String> wifi_results;
    private List<ScanResult> wifi_list;
    private JSONObject wifi_info,Signals;
    private JSONArray accessPoints;
    private SharedPreferences sharedPref;
    private Button gather,send,search;
    private TextView result;
    private EditText searchText;
    String ID;
    int i=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gather__send);
        sharedPref = getApplicationContext().getSharedPreferences("Preferences", 0);
        gather = (Button) findViewById(R.id.gather);
        send = (Button) findViewById(R.id.send);
        search = (Button) findViewById(R.id.search);
        gather.setOnClickListener(this);
        send.setOnClickListener(this);
        search.setOnClickListener(this);
        result = (TextView) findViewById(R.id.response);
        searchText = (EditText) findViewById(R.id.searchText);
        wifi_receiver = new WifiReceiver();
        wifi_manager = (WifiManager)this.getApplicationContext().getSystemService(this.WIFI_SERVICE);
        info = wifi_manager.getConnectionInfo();
        wifi_results = new ArrayList<String>();
        Signals = new JSONObject();

        scanWifi(0);
    }
    public void scanWifi(int i){
        this.i = i;
        wifi_manager.startScan();
    }
    protected void onResume(){
        super.onResume();
        // Register wifi receiver to get the results
        registerReceiver(wifi_receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

    }

    protected void onPause(){
        super.onPause();
        // Unregister the wifi receiver
        unregisterReceiver(wifi_receiver);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.gather:
                Signals = new JSONObject();
                scanWifi(0);
                break;
            case R.id.send:
                JSONObject Data = new JSONObject();
                ID = sharedPref.getString("UserID","");
                try {
                    Data.put("Signals",Signals);
                    Data.put("UserID",ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Send send = new Send();
                send.Send(this,Data);
                Log.wtf("Send",Data.toString());
                break;
            case R.id.search:

                String query = searchText.getText().toString();
                if(!query.isEmpty()){
                    Log.wtf("Gather","search");
                    Locate locate =new Locate();
                    locate.Locate(this,query);
                }else {
                    Toast.makeText(Gather.this,"Empty Search Query",Toast.LENGTH_SHORT);
                }
                break;
        }
    }
    public void setText(String text){
        result.setText(text);
    }
    class WifiReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            wifi_results.clear();
            wifi_list = wifi_manager.getScanResults();
            accessPoints = new JSONArray();
            i++;
            for (ScanResult result:wifi_list) {
                wifi_results.add(result.toString());
                wifi_info = new JSONObject();
                try {
                    wifi_info.put("BSSID",result.BSSID.toString());
                    wifi_info.put("RSS",result.level);
                    accessPoints.put(wifi_info);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            setText(accessPoints.toString());
            try {
                Signals.put("Signals".concat(String.valueOf(i)),accessPoints);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(i%10!=0 && i<10){
                scanWifi(i%10);
            }
        }

    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder server_builder = new AlertDialog.Builder(Gather.this);
        View server_dialog = getLayoutInflater().inflate(R.layout.server_dialog,null);
        final EditText server_field = (EditText) server_dialog.findViewById(R.id.server_field);

        switch (item.getItemId()){
            case R.id.show_server:
                server_builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_field.setText(sharedPref.getString("Server_Address",""));
                server_field.setEnabled(false);
                server_builder.setView(server_dialog);
                final AlertDialog show_dialog = server_builder.create();
                show_dialog.show();

                return true;
            case R.id.change_server:
                server_field.setText(sharedPref.getString("Server_Address",""));
                server_builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                server_builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_builder.setView(server_dialog);
                final AlertDialog alert_dialog = server_builder.create();
                alert_dialog.show();

                alert_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String server_address = server_field.getText().toString();
                        if(!server_address.isEmpty() ){

                            changeServer(server_address);

                        }else{
                            Toast.makeText(Gather.this, "Server address not changed", Toast.LENGTH_SHORT).show();
                        }
                        alert_dialog.dismiss();
                    }
                });
                return true;
            case R.id.logout:
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("LoginStat", false);
                editor.apply();
                Intent loginIntent = new Intent(this,login.class);
                startActivity(loginIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
    public void changeServer(String server){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Server_Address", server);
        editor.apply();
    }
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
