package com.example.zyraphyl.userlocator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Gather_Send extends AppCompatActivity implements View.OnClickListener {
    private SharedPreferences sharedPref;
    private Button gather,send,search;
    private TextView result;
    private EditText searchText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gather__send);
        sharedPref = getApplicationContext().getSharedPreferences("Preferences", 0);
        gather = (Button) findViewById(R.id.gather);
        send = (Button) findViewById(R.id.send);
        search = (Button) findViewById(R.id.search);
        gather.setOnClickListener(this);
        send.setOnClickListener(this);
        search.setOnClickListener(this);
        searchText = (EditText) findViewById(R.id.searchText);
        result = (TextView) findViewById(R.id.response);
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder server_builder = new AlertDialog.Builder(Gather_Send.this);
        View server_dialog = getLayoutInflater().inflate(R.layout.server_dialog,null);
        final EditText server_field = (EditText) server_dialog.findViewById(R.id.server_field);

        switch (item.getItemId()){
            case R.id.show_server:
                server_builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_field.setText(sharedPref.getString("Server_Address",""));
                server_field.setEnabled(false);
                server_builder.setView(server_dialog);
                final AlertDialog show_dialog = server_builder.create();
                show_dialog.show();

                return true;
            case R.id.change_server:
                server_field.setText(sharedPref.getString("Server_Address",""));
                server_builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                server_builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_builder.setView(server_dialog);
                final AlertDialog alert_dialog = server_builder.create();
                alert_dialog.show();

                alert_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String server_address = server_field.getText().toString();
                        if(!server_address.isEmpty() ){

                            changeServer(server_address);

                        }else{
                            Toast.makeText(Gather_Send.this, "Server address not changed", Toast.LENGTH_SHORT).show();
                        }
                        alert_dialog.dismiss();
                    }
                });
                return true;
            case R.id.logout:
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("LoginStat", false);
                editor.apply();
                Intent loginIntent = new Intent(this,login.class);
                startActivity(loginIntent);
//                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
    public void changeServer(String server){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Server_Address", server);
        editor.apply();
    }
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.gather:
                Intent gatherIntent = new Intent(this,Gather.class);
                startActivity(gatherIntent);
                break;
            case R.id.send:
//                Send send = new Send();
//                send.Send(this);
                Toast.makeText(Gather_Send.this,"No data to be sent",Toast.LENGTH_LONG).show();
                break;
            case R.id.search:
                Log.wtf("Gather_send","Search");
                String query = searchText.getText().toString();
                if(!query.isEmpty()){
                    Locate locate =new Locate();
                    locate.Locate(this,query);
                }else {
                    Toast.makeText(Gather_Send.this,"Empty Search Query",Toast.LENGTH_SHORT);
                }
                break;
        }
    }
}
