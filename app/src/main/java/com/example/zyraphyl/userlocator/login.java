package com.example.zyraphyl.userlocator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class login extends AppCompatActivity {
    private Button login;
    private EditText userID,password;
    private SharedPreferences sharedPref;
    String id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (Button) findViewById(R.id.login);
        userID = (EditText) findViewById(R.id.userID);
        password = (EditText) findViewById(R.id.userPassword);
        sharedPref = getApplicationContext().getSharedPreferences("Preferences", 0);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitToServer(v);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.server,menu);
        return true;
    }
    private void submitToServer(View v) {
        final String json = createJson();
        new AsyncTask<Void,Void,String>(){

            @Override
            protected String doInBackground(Void... voids) {
                return sendData(json);
            }

            @Override
            protected void onPostExecute(String s) {
                Toast.makeText(login.this, s, Toast.LENGTH_LONG).show();

                if(s.equalsIgnoreCase("success")){
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("LoginStat", true);
                    editor.putString("UserID",id);
                    editor.apply();
                    Log.wtf("login",s);
                    finishAct();
                }
            }
        }.execute();

    }

    public void finishAct() {
//        finishActivity(1);
        finish();
        return;
    }

    public String createJson(){
        id = userID.getText().toString();
        String pass = password.getText().toString();
        JSONObject json = new JSONObject();
        if(!id.isEmpty() && !pass.isEmpty()){
            try {
                json.put("user_ID",id);
                json.put("Password",pass);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.wtf("login",json.toString());
            return json.toString();
        }else{
            Toast.makeText(login.this, "Please fill in the fields", Toast.LENGTH_SHORT).show();
        }
        return null;
    }
    private String sendData(String json){

        String address = sharedPref.getString("Server_Address","");
        if (address!=null) {
            address=address.concat("/login.php");
            HttpPost post = new HttpPost(address);
            try {
                Log.wtf("sendData", "sent");
                StringEntity stringEntity = new StringEntity(json);
                post.setEntity(stringEntity);
                post.setHeader("Content-type", "application/json");

                DefaultHttpClient client = new DefaultHttpClient();
                BasicResponseHandler handler = new BasicResponseHandler();

                String response = client.execute(post, handler);
                Log.wtf("sendData", response);
                return response;
            } catch (UnsupportedEncodingException e) {
                Log.d("SendingJson", e.toString());
            } catch (ClientProtocolException e) {
                Log.d("SendingJson", e.toString());
            } catch (IOException e) {
                Log.d("SendingJson", e.toString());
            }
            return "Unable to connect to server";
        }else{
            Toast.makeText(this,"Initiate Server Address",Toast.LENGTH_SHORT);
            return "Set Server";
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder server_builder = new AlertDialog.Builder(login.this);
        View server_dialog = getLayoutInflater().inflate(R.layout.server_dialog,null);
        final EditText server_field = (EditText) server_dialog.findViewById(R.id.server_field);

        switch (item.getItemId()){
            case R.id.showServer:
                server_builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_field.setText(sharedPref.getString("Server_Address",""));
                server_field.setEnabled(false);
                server_builder.setView(server_dialog);
                final AlertDialog show_dialog = server_builder.create();
                show_dialog.show();

                return true;
            case R.id.changeServer:
                server_field.setText(sharedPref.getString("Server_Address",""));
                server_builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                server_builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_builder.setView(server_dialog);
                final AlertDialog alert_dialog = server_builder.create();
                alert_dialog.show();

                alert_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String server_address = server_field.getText().toString();
                        if(!server_address.isEmpty() ){

                            changeServer(server_address);

                        }else{
                            Toast.makeText(login.this, "Server address not changed", Toast.LENGTH_SHORT).show();
                        }
                        alert_dialog.dismiss();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
    public void changeServer(String server){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Server_Address", server);
        editor.apply();
    }
    public void onBackPressed() {
        moveTaskToBack(true);

    }
}
