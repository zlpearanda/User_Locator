package com.example.zyraphyl.userlocator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPref = getApplicationContext().getSharedPreferences("Preferences", 0);
        checkLogin();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkLogin();
    }

    public void checkLogin(){
        Log.wtf("Main", String.valueOf(sharedPref.getBoolean("LoginStat",false)));
        if(!sharedPref.getBoolean("LoginStat",false)){
            Intent loginIntent = new Intent(this,login.class);
            startActivity(loginIntent);
        }else {
            Intent gatherSendIntent = new Intent(this,Gather_Send.class);
            startActivity(gatherSendIntent);
        }
    }

}
