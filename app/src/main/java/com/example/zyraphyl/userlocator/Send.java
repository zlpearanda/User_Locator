package com.example.zyraphyl.userlocator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Send{
    private JSONObject signals;
    private SharedPreferences sharedPref ;
    private Context context;
    private JSONObject Signals;

    public void Send(Context context, JSONObject Signals){
        sharedPref = context.getSharedPreferences("Preferences", 0);
        this.context = context;
        this.Signals = Signals;
        submitToServer();
    }
    public String sendData(String json){
        String address = sharedPref.getString("Server_Address",null).concat("/setLocation.php");
        HttpPost post = new HttpPost(address);
        try {
            Log.wtf("sendData","sent");
            StringEntity stringEntity = new StringEntity(json);
            post.setEntity(stringEntity);
            post.setHeader("Content-type","application/json");

            DefaultHttpClient client = new DefaultHttpClient();
            BasicResponseHandler handler = new BasicResponseHandler();
            Log.wtf("sendData","hhere");
            String resp = client.execute(post,handler);
            Log.wtf("sendData","response:".concat(resp));
            return resp;
        } catch (UnsupportedEncodingException e) {
            Log.d("SendingJson",e.toString());
        } catch (ClientProtocolException e) {
            Log.d("SendingJson",e.toString());
        } catch (IOException e) {
            Log.d("SendingJson",e.toString());
        }
        return "Unable to connect to server";

    }
    private void submitToServer() {
//        String json = "";
//        json = createJson();

//        Log.wtf("send",json);
        final String finalJson = Signals.toString();
//        Log.wtf("Send",finalJson);
//        final String finalJson = json;
        new AsyncTask<Void,Void,String>(){

            @Override
            protected String doInBackground(Void... voids) {
                return sendData(finalJson);
            }

            @Override
            protected void onPostExecute(String s) {
                setText(s);

            }
        }.execute();

    }
    public void setText(String text){
        TextView txtView = (TextView) ((Activity)context).findViewById(R.id.response);
        txtView.setText(text);
    }
//    public String createJson(){
//        ArrayList<String> BSSID = new ArrayList();
//        ArrayList<Integer> RSS = new ArrayList();
////        BSSID.add("c8:d7:19:e7:ad:77");
////        RSS.add(-97);
////        BSSID.add("14:91:82:79:69:3f");
////        RSS.add(-74);
////        BSSID.add("b4:5d:50:b6:a2:81");
////        RSS.add(-89);
////        BSSID.add("b4:5d:50:b6:a2:80");
////        RSS.add(-89);
////        BSSID.add("b4:5d:50:b6:a2:82");
////        RSS.add(-89);
//        BSSID.add("c0:56:27:40:62:1f");
//        RSS.add(-79);
//        BSSID.add("b4:5d:50:b6:a3:e1");
//        RSS.add(-75);
//        BSSID.add("5c:31:3e:d5:47:e4");
//        RSS.add(-74);
//        BSSID.add("b4:5d:50:b6:a3:e0");
//        RSS.add(-75);
//        BSSID.add("b4:5d:50:b6:a3:e2");
//        RSS.add(-75);
////
////        BSSID.add("00:13:33:cc:4d:df");
////        RSS.add(-96);
//        BSSID.add("98:de:d0:7d:4f:4d");
//        RSS.add(-97);
//        BSSID.add("c4:e9:84:4f:04:f4");
//        RSS.add(-96);
//        BSSID.add("70:4f:57:d0:f7:86");
//        RSS.add(-95);
////        BSSID.add("b4:5d:50:b6:7b:01");
////        RSS.add(-95);
//        Log.wtf("send", String.valueOf(BSSID));
//        JSONArray jsonArray = new JSONArray();
//        JSONObject json = new JSONObject();
//        signals = new JSONObject();
//        for (int i=0; i<BSSID.size(); i+=1) {
//            json = new JSONObject();
//            try {
//                json.put("BSSID",BSSID.get(i));
//                json.put("RSS",RSS.get(i));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            jsonArray.put(json);
//        }
//        try {
//            signals.put("Signals",jsonArray);
//        }catch (JSONException e){
//            e.printStackTrace();
//        }
//
//
//        return signals.toString();
//    }


}
